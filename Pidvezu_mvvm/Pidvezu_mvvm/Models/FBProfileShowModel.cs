﻿using Xamarin.Forms;
using Prism.Mvvm;

namespace Pidvezu_mvvm.Models
{
    public class FBProfileShowModel : BindableBase
    {
        private string _name;
        private string _employment;
        private string _phoneNumber;
        private string _age;
        private string _carInformation;
        private string _status;
        private ImageSource _profileImage;

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { SetProperty(ref _phoneNumber, value); }
        }
        public string Age
        {
            get { return _age; }
            set { SetProperty(ref _age, value); }
        }
        public string CarInformation
        {
            get { return _carInformation; }
            set { SetProperty(ref _carInformation, value); }
        }
        public string Employment
        {
            get { return _employment; }
            set { SetProperty(ref _employment, value); }
        }
        public string Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }
        public ImageSource ProfileImage
        {
            get { return _profileImage; }
            set { SetProperty(ref _profileImage, value); }
        }
    }
}
