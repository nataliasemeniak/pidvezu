﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;
using Xamarin.Forms;
using System.ComponentModel;

namespace Pidvezu_mvvm.Models
{
    class ShowDriversModel:  INotifyPropertyChanged
    {
        private string _name, _occupation;
        private ImageSource _photo;

        public string Occupation
        {
            get { return _occupation; }
            set { value = _occupation; OnPropertyChanged("Occupation"); }
        }
        public string Name
        {
            get { return _name; }
            set { value = _name; OnPropertyChanged("Name"); }
        }
        public ImageSource Photo
        {
            get { return _photo; }
            set { value = _photo; OnPropertyChanged("Photo"); }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
