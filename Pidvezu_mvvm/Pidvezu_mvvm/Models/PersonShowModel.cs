﻿using Prism.Mvvm;
using Xamarin.Forms;
namespace Pidvezu_mvvm.Models
{
    public class PersonShowModel : BindableBase
    {
        private string _name;
        private string _age;
        private string _employment;
        private string _route;
        private string _time;
        private string _сarInformation;
        private string _phoneNumber;
        private ImageSource _profileImage;
        private bool _isSaved;

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }
        public string Age
        {
            get { return _age; }
            set { SetProperty(ref _age, value); }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { SetProperty(ref _phoneNumber, value); }
        }
        public string CarInformation
        {
            get { return _сarInformation; }
            set { SetProperty(ref _сarInformation, value); }
        }

        public string Employment
        {
            get { return _employment; }
            set { SetProperty(ref _employment, value); }
        }
        public string Route
        {
            get { return _route; }
            set { SetProperty(ref _route, value); }
        }
        public string Time
        {
            get { return _time; }
            set { SetProperty(ref _time, value); }
        }

        public ImageSource ProfileImage
        {
            get { return _profileImage; }
            set { SetProperty(ref _profileImage, value); }
        }
        public bool IsSaved
        {
            get { return _isSaved; }
            set { SetProperty(ref _isSaved, value); }
        }


    }
}
