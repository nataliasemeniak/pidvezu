﻿using System;
using Xamarin.Forms;

namespace Pidvezu_mvvm.Views
{
    public partial class RegisteredUser_TabbedPage : TabbedPage
    {
        public RegisteredUser_TabbedPage()
        {
            InitializeComponent();
        }
        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();

            Title = CurrentPage?.Title;
        }
    }
}
