﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Pidvezu_mvvm.Views
{
    public partial class ListOfPassengersPage : ContentPage
    {
        public ListOfPassengersPage()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            var position = new Position(49.83826, 24.02324);

            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Lviv",
                Address = "Ukraine"
            };

            passengersMap.Pins.Add(pin);
            passengersMap.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromKilometers(10)));
        }
    }
}
