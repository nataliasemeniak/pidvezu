﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Pidvezu_mvvm.Views
{
    public partial class NonRegisteredUser_Passengers : ContentPage
    {
        public NonRegisteredUser_Passengers()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            var position = new Position(49.83826, 24.02324);

            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Lviv",
                Address = "Ukraine"
            };

            map.Pins.Add(pin);
            map.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromKilometers(10)));
        }
    }
}
