﻿using Xamarin.Forms;
using Pidvezu_mvvm.ViewModels;
using Prism.Navigation;
using Xamarin.Forms.Maps;

namespace Pidvezu_mvvm.Views
{
    public partial class ListOfDriversPage : ContentPage
    {
        public ListOfDriversPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var position = new Position(49.83826, 24.02324);

            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Lviv",
                Address = "Ukraine"
            };

            driversMap.Pins.Add(pin);
            driversMap.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromKilometers(10)));
        }
    }
}
