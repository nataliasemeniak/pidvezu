﻿using System;
using System.Threading.Tasks;
using Pidvezu_mvvm.ViewModels;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace Pidvezu_mvvm.Views
{
    public partial class EditProfilePage : ContentPage
    {
        private IPageDialogService _dialogService;
        private EditProfilePageViewModel vm => this.BindingContext as EditProfilePageViewModel;

        public EditProfilePage()
        {
            InitializeComponent();

        }

        protected  override  bool OnBackButtonPressed()
        {
           _dialogService.DisplayAlertAsync(null, "Зберегти зміни?", "Так", "Ні");
            vm.ShowUserDialogCommand.Execute(null);

            return true;
        }

    }
}
