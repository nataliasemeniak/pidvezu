﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pidvezu_mvvm
{
    public class GradientColorStack : StackLayout
    {
        public Color StartColor { get; set; }
        public Color EndColor { get; set; }
    }
}
