﻿using Prism.Unity;
using Pidvezu_mvvm.Views;
using Xamarin.Forms;

namespace Pidvezu_mvvm
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            InitializeComponent();
            
            NavigationService.NavigateAsync("NavigationPage/MainPage");
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<NavigationPage>();
            Container.RegisterTypeForNavigation<MainPage>();
            Container.RegisterTypeForNavigation<RegisteredUser_TabbedPage>();
            Container.RegisterTypeForNavigation<EditProfilePage>();

            
            Container.RegisterTypeForNavigation<FindPassengersPage>();
            Container.RegisterTypeForNavigation<FindDriversPage>();
            Container.RegisterTypeForNavigation<ListOfDriversPage>();
            Container.RegisterTypeForNavigation<ListOfPassengersPage>();

            Container.RegisterTypeForNavigation<NonRegisteredUser_FindDriversPage>();
            Container.RegisterTypeForNavigation<NonRegisteredUser_FindPassengersPage>();

            Container.RegisterTypeForNavigation<SignInPage>();
          
            Container.RegisterTypeForNavigation<NonRegisteredUser_Drivers>();
            Container.RegisterTypeForNavigation<NonRegisteredUser_Passengers>();
            Container.RegisterTypeForNavigation<ReviewProfilePage>();
            Container.RegisterTypeForNavigation<LoginWithFBPage>();
        }
    }
}
