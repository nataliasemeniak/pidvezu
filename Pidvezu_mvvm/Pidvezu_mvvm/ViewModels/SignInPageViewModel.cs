﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Views;

namespace Pidvezu_mvvm.ViewModels
{
    public class SignInPageViewModel : BindableBase, INavigatedAware
    {
        private INavigationService _navigationService;
	    private string _navigatedFromPageName;

        public SignInPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

		public ICommand SignInCommand {
			get {
				return new DelegateCommand(  () => {
				        switch (_navigatedFromPageName)
				        {
                            case nameof(MainPage):
                                _navigationService.NavigateAsync( "myapp:///NavigationPage/RegisteredUser_TabbedPage" );
				                break;
                            case nameof(NonRegisteredUser_FindDriversPage):
				                {
                                    _navigationService.NavigateAsync( "myapp:///NavigationPage/RegisteredUser_TabbedPage/FindDriversPage" );
                                }
				               break;
                            case nameof(NonRegisteredUser_Drivers):
				                {
                                    _navigationService.NavigateAsync( "myapp:///NavigationPage/RegisteredUser_TabbedPage/ListOfDriversPage" );
                                }
				                break;
                            case nameof(NonRegisteredUser_FindPassengersPage):
                                {
                                    _navigationService.NavigateAsync( "myapp:///NavigationPage/RegisteredUser_TabbedPage/FindPassengersPage" );
                                }
				                break;
                            case nameof(NonRegisteredUser_Passengers):
                                {
                                    _navigationService.NavigateAsync( "myapp:///NavigationPage/RegisteredUser_TabbedPage/ListOfPassengersPage" );
                                }
				                break;
                            default:
				                _navigationService.GoBackAsync();
				               break;

                         }
   

                } );
			}
		}

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey(ConstantHelper.NavigatedFromPageParameter))
            {
                _navigatedFromPageName = (string)parameters[ConstantHelper.NavigatedFromPageParameter];
            }
        }
    }
}
