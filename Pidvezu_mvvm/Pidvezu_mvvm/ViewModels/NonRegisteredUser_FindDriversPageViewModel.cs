﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Views;

namespace Pidvezu_mvvm.ViewModels
{
    public class NonRegisteredUser_FindDriversPageViewModel : BindableBase
    {
        private INavigationService _navigationService;
        public NonRegisteredUser_FindDriversPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public ICommand NRU_FindDriversButtonCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    _navigationService.NavigateAsync("NonRegisteredUser_Drivers"/*"RegisteredUser_TabbedPage"*/);
                });
            }
        }

        public ICommand GotoSignInPageCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/FindDriversPage");
                });
            }
        }
    }
}
