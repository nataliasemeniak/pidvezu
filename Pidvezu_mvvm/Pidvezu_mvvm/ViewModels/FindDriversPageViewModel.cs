﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System.Windows.Input;

namespace Pidvezu_mvvm.ViewModels
{
    public class FindDriversPageViewModel : BindableBase
    {
        private INavigationService _navigationService;

        public FindDriversPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public ICommand FindDriversButtonCommand
        {
            get { return new DelegateCommand(() =>
            {
                _navigationService.NavigateAsync("ListOfDriversPage");
            });}
        }
       
    }
}
