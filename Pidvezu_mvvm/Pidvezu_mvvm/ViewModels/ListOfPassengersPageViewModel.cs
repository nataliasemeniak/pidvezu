﻿using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Models;
using Prism.Commands;
using Prism.Navigation;


namespace Pidvezu_mvvm.ViewModels
{
    public class ListOfPassengersPageViewModel : BindableBase
    {
        private PersonShowModel _passengerSelected;
        private INavigationService _navigationService;
        private ObservableCollection<PersonShowModel> _passengersList;
        private ObservableCollection<SavedPassengerShowModel> _savedPassengersList;
        public ListOfPassengersPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            PassengersList = new ObservableCollection<PersonShowModel>() { };
            for (int i = 0; i < 20; ++i)
                PassengersList.Add(new PersonShowModel()
                {
                    Name = "TName",
                    Employment = "TEmployment",
                    ProfileImage = "profile.png",
                    Route = "City, streetFrom - Lviv, streetTo",
                    Time = "Start Time - End Time",
                    Age = "21",
                    PhoneNumber = "063 22 22 525",
                    CarInformation = "-",
                    IsSaved = false
                });
            SavedPassengersList = new ObservableCollection<SavedPassengerShowModel>();
        }
        public ObservableCollection<SavedPassengerShowModel> SavedPassengersList
        {
            get { return _savedPassengersList; }
            set { SetProperty(ref _savedPassengersList, value); }
        }
        public PersonShowModel PassengerSelected
        {
            get { return _passengerSelected; }
            set
            {
                SetProperty(ref _passengerSelected, value);

                if (_passengerSelected != null)
                {

                    var navigationParameters = new NavigationParameters();
                    navigationParameters.Add(ConstantHelper.NavigatedFromPageParameterSelectedItem, _passengerSelected);
                    _navigationService.NavigateAsync("ReviewProfilePage", navigationParameters);
                }
            }

        }

        public ICommand AddSavedPassengerRouteCommand
        {
            get
            {
                return new DelegateCommand<PersonShowModel>(item =>
                {
                    SavedPassengersList.Add(new SavedPassengerShowModel()
                    {
                        Name = item.Name,
                        Age = item.Age,
                        CarInformation = item.CarInformation,
                        PhoneNumber = item.PhoneNumber,
                        Employment = item.Employment,
                        ProfileImage= item.ProfileImage,
                        Route = item.Route,
                        Time = item.Time
                    });
                    var navigationParameters = new NavigationParameters();
                    navigationParameters.Add(ConstantHelper.NavigateToSavedPassengerListParameter, SavedPassengersList);
                    _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/ListOfDriversPage", navigationParameters);


                });
            }
        }
        public ObservableCollection<PersonShowModel> PassengersList
        {
            get { return _passengersList; }
            set { SetProperty(ref _passengersList, value); }
        }
    }
}
