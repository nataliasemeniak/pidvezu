﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Views;

namespace Pidvezu_mvvm.ViewModels
{
    public class MainPageViewModel : BindableBase {
	    private INavigationService _navigationService;

        public MainPageViewModel(INavigationService navigationService) {
	        _navigationService = navigationService;
        }

	    public ICommand NRU_GoToSearchForDriversCommand {
		    get {
			    return new DelegateCommand( () =>
			    {
			        _navigationService.NavigateAsync(
			            "NonRegisteredUser_FindDriversPage");
			    } );
		    }
		}
        
		public ICommand NRU_GoToSearchForPassengersCommand {
			get {
				return new DelegateCommand( () => {
                    _navigationService.NavigateAsync(
                       "NonRegisteredUser_FindPassengersPage");
                } );
			}
		}


        public ICommand GoToSignInRegistrationCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage");
                });
            }
        }

    }
}
