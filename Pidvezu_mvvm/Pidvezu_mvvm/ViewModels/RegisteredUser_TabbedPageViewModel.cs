﻿using System;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Pidvezu_mvvm.Models;
using Prism.Navigation;
using System.ComponentModel;
using Pidvezu_mvvm;
using System.Linq;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Views;
using Xamarin.Forms;


namespace Pidvezu_mvvm.ViewModels
{
    public class RegisteredUser_TabbedPageViewModel : BindableBase, INavigatedAware
    {
        private ObservableCollection<SavedPassengerShowModel> _savedPassengersList;
        private ObservableCollection<SavedDriverShowModel> _savedDriversList;
        private FBProfileShowModel _profile;
        private SavedDriverShowModel _selectedDriverItem;
        private SavedPassengerShowModel _selectedPassengerItem;

     
        public INavigationService _navigationService;

        public RegisteredUser_TabbedPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            SavedPassengersList = new ObservableCollection<SavedPassengerShowModel>() {};
            for (int i = 0; i < 4; ++i)
                SavedPassengersList.Add(new SavedPassengerShowModel()
                {
                    ProfileImage = "profile.png",
                    Name = "TName",
                    Route = "City, streetFrom - Lviv, streetTo",
                    Time = "Start Time - End Time",
                    Employment = "TEmployment",
                    Age = "TAge",
                    CarInformation = "TCarInformation"
                });
            SavedDriversList = new ObservableCollection<SavedDriverShowModel>() {};
            for (int i = 0; i < 3; ++i)
                SavedDriversList.Add(new SavedDriverShowModel()
                {
                    ProfileImage = "profile.png",
                    Name = "TName",
                    Route = "City, streetFrom - Lviv, streetTo",
                    Time = "Start Time - End Time",
                    Employment = "TEmployment",
                    Age = "TAge",
                    CarInformation = "TCarInformation"
                });
           
            Profile = new FBProfileShowModel()
            {
                Name = "Олег",
                Age = "32",
                CarInformation = "Opel Vectra",
                PhoneNumber = "067 99 88 553",
                Employment = "Менеджер",
                ProfileImage = "profile.png"
            };
           
        }


        public ObservableCollection<SavedPassengerShowModel> SavedPassengersList
        {
            get { return _savedPassengersList; }
            set { SetProperty(ref _savedPassengersList, value); }
        }

        public ObservableCollection<SavedDriverShowModel> SavedDriversList
        {
            get { return _savedDriversList; }
            set { SetProperty(ref _savedDriversList, value); }
        }

        public FBProfileShowModel Profile
        {
            get { return _profile; }
            set { SetProperty(ref _profile, value); }
        }

        public ICommand GoToEditProfilePageCommand
        {
            get
            {
                return
                    new DelegateCommand(() =>
                    {
                        var navigationParameters = new NavigationParameters();
                        navigationParameters.Add(ConstantHelper.ProfileParameter, Profile);
                        _navigationService.NavigateAsync("EditProfilePage", navigationParameters);
                    });
            }
        }

        public ICommand GoToSearchForDriversCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    _navigationService.NavigateAsync("FindDriversPage");
                });
            }
        }

        public ICommand GoToSearchForPassengersCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    _navigationService.NavigateAsync("FindPassengersPage");
                });
            }
        }

        public ICommand ExitButtonCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    _navigationService.NavigateAsync("MainPage");
                });
            }
        }

        public ICommand DeleteDriverSavedItem
        {
            get
            {
                return new DelegateCommand<SavedDriverShowModel>( item => 
				{
	                SavedDriversList.Remove( item );
                });


            }
        }
        public ICommand DeletePassengerSavedItem
        {
            get
            {
                return new DelegateCommand<SavedPassengerShowModel>(item =>
                {
                    SavedPassengersList.Remove(item);
                });


            }
        }
        public SavedDriverShowModel SelectedDriverItem
        {
            get { return _selectedDriverItem; }
            set
            {
                SetProperty(ref _selectedDriverItem, value);

                if (_selectedDriverItem != null)
                {

                    var navigationParameters = new NavigationParameters();
                    navigationParameters.Add(ConstantHelper.NavigatedFromPageParameterSelectedItem, _selectedDriverItem);
                    _navigationService.NavigateAsync("ReviewProfilePage", navigationParameters);
                }
            }
        }
        public SavedPassengerShowModel SelectedPassengerItem
        {
            get { return _selectedPassengerItem; }
            set
            {
                SetProperty(ref _selectedPassengerItem, value);

                if (_selectedPassengerItem != null)
                {

                    var navigationParameters = new NavigationParameters();
                    navigationParameters.Add(ConstantHelper.NavigatedFromPageParameterSelectedItem, _selectedPassengerItem);
                    _navigationService.NavigateAsync("ReviewProfilePage", navigationParameters);
                }
            }
        }
	    public void OnNavigatedFrom ( NavigationParameters parameters ) {
	    }

	    public void OnNavigatedTo ( NavigationParameters parameters ) {
			if( parameters.ContainsKey( ConstantHelper.ProfileParameter ) ) {
				Profile = (FBProfileShowModel) parameters[ConstantHelper.ProfileParameter];
			}
            if (parameters.ContainsKey(ConstantHelper.NavigateToSavedDriverListParameter))
            {
                SavedDriversList =
                    (ObservableCollection<SavedDriverShowModel>)
                        parameters[ConstantHelper.NavigateToSavedDriverListParameter];
              }
            if (parameters.ContainsKey(ConstantHelper.NavigateToSavedPassengerListParameter))
            {
                SavedPassengersList =
                    (ObservableCollection<SavedPassengerShowModel>)
                        parameters[ConstantHelper.NavigateToSavedPassengerListParameter];
            }
        }
    }

}
