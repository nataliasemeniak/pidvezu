﻿using System.Collections.ObjectModel;
using Prism.Commands;
using Prism.Mvvm;
using System.ComponentModel;
using Prism.Navigation;
using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Models;

namespace Pidvezu_mvvm.ViewModels
{
    public class ReviewProfilePageViewModel : BindableBase, INavigatedAware
    {
        public INavigationService _navigationService;
        private PersonShowModel _personSelected;
     
        public ReviewProfilePageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }
        public PersonShowModel PersonSelected
        {
            get { return _personSelected; }
            set{ SetProperty(ref _personSelected, value);}
        }
        public ICommand ImgClickCommand
        {
            get { return new DelegateCommand(() =>
            {
                _navigationService.NavigateAsync("MainPage");
            });}
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey(ConstantHelper.NavigatedFromPageParameterSelectedItem))
            {
                PersonSelected = (PersonShowModel)parameters[ConstantHelper.NavigatedFromPageParameterSelectedItem];
            }
        }

    }
}
