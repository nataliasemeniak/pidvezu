﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Models;
using Pidvezu_mvvm.Views;

namespace Pidvezu_mvvm.ViewModels
{
    public class NonRegisteredUser_PassengersViewModel : BindableBase
    {
        private INavigationService _navigationService;
        private ObservableCollection<PersonShowModel> _passengersList;
        public NonRegisteredUser_PassengersViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            PassengersList = new ObservableCollection<PersonShowModel>() { };
            for (int i = 0; i < 100; i++)
            {
                PassengersList.Add(new PersonShowModel()
                {
                    Name = "TName",
                    Employment = "TEmployment",
                    ProfileImage = "star.png"
                });
            }
        }
        public ObservableCollection<PersonShowModel> PassengersList
        {
            get { return _passengersList; }
            set { SetProperty(ref _passengersList, value); }
        }

        public ICommand GotoSignInPageCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/ListOfPassengersPage");
                });

            }
        }
        
    }
}
