﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Models;
using Pidvezu_mvvm.Views;
using Xamarin.Forms;

namespace Pidvezu_mvvm.ViewModels {
    public class NonRegisteredUser_DriversViewModel : BindableBase {
        private INavigationService _navigationService;
        private ObservableCollection<PersonShowModel> _driversList;

        public NonRegisteredUser_DriversViewModel ( INavigationService navigationService ) {
            _navigationService = navigationService;
            DriversList = new ObservableCollection<PersonShowModel>() { };
            for ( int i = 0; i < 100; i++ ) {
                DriversList.Add(new PersonShowModel() {
                    Name = "TName",
                    Employment = "TEmployment",
                    ProfileImage = "star.png"
                });
            }
        }

        public ObservableCollection<PersonShowModel> DriversList {
            get { return _driversList; }
            set { SetProperty(ref _driversList, value); }
        }

        public ICommand GotoSignInPageCommand {
            get {
                return new DelegateCommand(() => {
                    _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/ListOfDriversPage");
                });
            }
        }

    }
}
