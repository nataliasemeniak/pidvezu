﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System.Windows.Input;



namespace Pidvezu_mvvm.ViewModels
{
    public class FindPassengersPageViewModel : BindableBase
    {
        private INavigationService _navigationService;
        public FindPassengersPageViewModel( INavigationService navigationService)
        {
            _navigationService = navigationService;         
        }

        public ICommand FindPassengersButtonCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    _navigationService.NavigateAsync("ListOfPassengersPage");
                });
            }
        }
       
    }
}
