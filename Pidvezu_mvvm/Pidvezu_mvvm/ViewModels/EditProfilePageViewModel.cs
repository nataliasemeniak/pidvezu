﻿using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System.Collections.ObjectModel;
using System.Linq;


namespace Pidvezu_mvvm.ViewModels
{
    
    public class EditProfilePageViewModel : BindableBase, INavigatedAware
    {
        private INavigationService _navigationService;
        private IPageDialogService _dialogService;
        private FBProfileShowModel _profile;
        private ObservableCollection<string> itemsCollection;
        private string _selectedItem;


        public EditProfilePageViewModel(INavigationService navigationService,  IPageDialogService dialogService)
        {
            _navigationService = navigationService;
            _dialogService = dialogService;

            ItemsCollection = new ObservableCollection<string> {};
            ItemsCollection.Add("Водій");
            ItemsCollection.Add("Пасажир");


        }

        public string SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                SetProperty(ref _selectedItem, value);

            
            }
        }

        public ObservableCollection<string> ItemsCollection
        {
            get { return itemsCollection; }
            set
            {
                SetProperty(ref itemsCollection, value);
              
            }
        }
        public ICommand ShowUserDialogCommand
        {
            get
            {
                return new DelegateCommand(() =>
          {
              _navigationService.GoBackAsync();
          });
            }
        }
        public FBProfileShowModel Profile
        {
            get { return _profile; }
            set { SetProperty(ref _profile, value); }
        }
        public ICommand SaveButtonCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    Profile.Status = SelectedItem;
                    var navigationParameters = new NavigationParameters();
                    navigationParameters.Add(ConstantHelper.ProfileParameter, Profile);
                    _navigationService.GoBackAsync();
                });
            }
        }
        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey(ConstantHelper.ProfileParameter))
            {
               
                Profile = (FBProfileShowModel)parameters[ConstantHelper.ProfileParameter];
            }
        }

    }
}
