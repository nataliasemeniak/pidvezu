﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Models;
using Pidvezu_mvvm.Views;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Pidvezu_mvvm.ViewModels
{
    public class LoginWithFBPageViewModel : BindableBase, INavigatedAware
    {
        public INavigationService _navigationService;
        private string _navigatedFromPageName;
        private string _profileName;
        private ImageSource _profileImage;

        private ObservableCollection<FBProfileShowModel> _profile; 
        public LoginWithFBPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            Profile = new ObservableCollection<FBProfileShowModel>() {};
            Profile.Add(new FBProfileShowModel()
            {
                Name = "Продовжити як " + "Олег",
                ProfileImage = "profile.png"
            });
            _profileName = Profile.Select(x => x.Name).FirstOrDefault();
            _profileImage = Profile.Select(x => x.ProfileImage).FirstOrDefault();
        }

        public string Name
        {
            get { return _profileName; }
            set { SetProperty(ref _profileName, value); }   
        }
        public ImageSource Image
        {
            get { return _profileImage; }
            set { SetProperty(ref _profileImage, value); }
        }
        public ICommand ContinueAsButtonCommand
        {
            get { return new DelegateCommand(() =>
            {
                switch (_navigatedFromPageName)
                {
                    case nameof(MainPage):
                        _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage");
                        break;
                    case nameof(NonRegisteredUser_FindDriversPage):
                        {
                            _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/FindDriversPage");
                        }
                        break;
                    case nameof(NonRegisteredUser_Drivers):
                        {
                            _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/ListOfDriversPage");
                        }
                        break;
                    case nameof(NonRegisteredUser_FindPassengersPage):
                        {
                            _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/FindPassengersPage");
                        }
                        break;
                    case nameof(NonRegisteredUser_Passengers):
                        {
                            _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/ListOfPassengersPage");
                        }
                        break;
                    default:
                        _navigationService.GoBackAsync();
                        break;

                }
            });} 
        }

        public ObservableCollection<FBProfileShowModel> Profile
        {
            get { return _profile;}
            set { SetProperty(ref _profile, value); }
        } 
        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey(ConstantHelper.NavigatedFromPageParameter))
            {
                _navigatedFromPageName = (string)parameters[ConstantHelper.NavigatedFromPageParameter];
            }
        }
    }
}
