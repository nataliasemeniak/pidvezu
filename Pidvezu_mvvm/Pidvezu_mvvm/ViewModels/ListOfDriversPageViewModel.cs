﻿using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Pidvezu_mvvm.Helpers;
using Pidvezu_mvvm.Models;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Forms.Maps;

namespace Pidvezu_mvvm.ViewModels
{
    public class ListOfDriversPageViewModel : BindableBase
    { 
        private INavigationService _navigationService;
        private ObservableCollection<PersonShowModel> _driversList;
        private PersonShowModel _driverSelected;
        private ObservableCollection<SavedDriverShowModel> _savedDriversList;
        

        public ListOfDriversPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            _driversList = new ObservableCollection<PersonShowModel>();
            for (int i = 0; i < 2; ++i)
                _driversList.Add(new PersonShowModel()
                {
                    Name = "Roman",
                    Employment = "TEmployment",
                    ProfileImage = "profile.png",
                    Route = "City, streetFrom - Lviv, streetTo",
                    Time = "Start Time - End Time",
                    Age = "32",
                    PhoneNumber = "063 55 55 555",
                    CarInformation = "Volvo CX90",
                    IsSaved = false
                });
            _driversList.Add(new PersonShowModel()
            {
                Name = "Mykhaylo",
                Employment = "TEmployment",
                ProfileImage = "profile.png",
                Route = "City, streetFrom - Lviv, streetTo",
                Time = "Start Time - End Time",
                Age = "32",
                PhoneNumber = "063 55 55 555",
                CarInformation = "Volvo CX90",
                IsSaved = false
            });
            SavedDriversList = new ObservableCollection<SavedDriverShowModel>();
           
        }
        public ObservableCollection<SavedDriverShowModel> SavedDriversList
        {
            get { return _savedDriversList; }
            set { SetProperty(ref _savedDriversList, value); }
        }
        public PersonShowModel DriverSelected
        {
            get { return _driverSelected; }
            set
            {
                SetProperty(ref _driverSelected, value);

                if (_driverSelected != null)
                {

                    var navigationParameters = new NavigationParameters();
                    navigationParameters.Add(ConstantHelper.NavigatedFromPageParameterSelectedItem, _driverSelected);
                    _navigationService.NavigateAsync("ReviewProfilePage", navigationParameters);
                }
            }

        }        
       
        public ObservableCollection<PersonShowModel> DriversList
        {
            get { return _driversList; }
            set { SetProperty(ref _driversList, value); }
        }
        public ICommand AddSavedDriverRouteCommand
        {
            get
            {
                return new DelegateCommand<PersonShowModel>(item =>
                {
                   
                       
                        SavedDriversList.Add(new SavedDriverShowModel()
                        {
                            Name = item.Name,
                            Age = item.Age,
                            CarInformation = item.CarInformation,
                            PhoneNumber = item.PhoneNumber,
                            Employment = item.Employment,
                            ProfileImage = item.ProfileImage,
                            Route = item.Route,
                            Time = item.Time
                        });
                        var navigationParameters = new NavigationParameters();
                        navigationParameters.Add(ConstantHelper.NavigateToSavedDriverListParameter, SavedDriversList);
                        _navigationService.NavigateAsync("myapp:///NavigationPage/RegisteredUser_TabbedPage/ListOfDriversPage", navigationParameters);
                    
                });
            }
        }
    }
}
