﻿using Pidvezu_mvvm.Models;

namespace Pidvezu_mvvm.Helpers {
	public class ConstantHelper {
		public static string NavigatedFromPageParameter = "NavigatedFromPageParameter";
		public static string ProfileParameter = "ProfileParameter";
		public static string NavigatedFromPageParameterSelectedItem = "selectedItem";
        public static string NavigateToSavedDriverListParameter = "savedDriverItem";
        public static string NavigateToSavedPassengerListParameter = "savedPassengerItem";
    }
}
